## sdm660_64-user 10 QKQ1.191014.001 eng.root.20201016.161857 release-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: r2p
- Brand: OPPO
- Flavor: aosp_r2p-userdebug
- Release Version: 12
- Id: SQ1D.211205.016.A1
- Incremental: 1640885799
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OPPO/RMX1801/RMX1801:10/QKQ1.191014.001/1602573502:user/release-keys
- OTA version: 
- Branch: sdm660_64-user-10-QKQ1.191014.001-eng.root.20201016.161857-release-keys
- Repo: oppo_r2p_dump_24132


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
